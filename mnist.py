import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import torchvision
import torchvision.transforms as transforms
from vision_transformer import VisionTransformer

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

batch_size = 256

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5,), (0.5,))])

mnist_train_dataset, mnist_test_dataset = (
    torchvision.datasets.MNIST(
        './mnist-data', train=x, download=True, transform=transform) for x in [True, False])

mnist_train_loader, mnist_test_loader = (
    torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=(dataset == mnist_train_dataset))
    for dataset in (mnist_train_dataset, mnist_test_dataset)
)

sample_image_batch, sample_label_batch = next(iter(mnist_train_loader))
image_shape = sample_image_batch.shape[1:]
print(image_shape)
plt.imshow(sample_image_batch[0].permute(1, 2, 0))
plt.title(sample_label_batch[0])
plt.savefig('sample-image.png')

classifier = VisionTransformer((1, 28, 28), 7, 10, 10, 3, 2, use_premade_positioning=True).to(device)

# classifier.load_state_dict(torch.load('./classifier.pt'))

loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(classifier.parameters(), lr=1e-3)

max_eps = 10

for ep in range(max_eps):
    for i, data in enumerate(mnist_train_loader):
        image_batch, label_batch = (x.to(device) for x in data)
        for param in classifier.parameters():
            param.grad = None
        y_pred = classifier(image_batch)
        loss = loss_fn(y_pred, label_batch)
        loss.backward()
        optimizer.step()
        acc = (torch.argmax(y_pred, 1) == label_batch).sum() / batch_size
        if i % 10 == 9:
            print(f'[{ep + 1}][{i + 1}] loss: {loss.item()}, acc: {acc}')

correct = 0
total = 0
with torch.no_grad():
    for data in mnist_test_loader:
        images, labels = (x.to(device) for x in data)
        outputs = classifier(images)
        predicted = torch.argmax(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

print(f'Accuracy of the network on the test images: {100 * correct // total}%')

torch.save(classifier.state_dict(), 'classifier.pt')

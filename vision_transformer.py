import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class TransformerBlock(nn.Module):
    def __init__(self, embed_size, num_heads, forward_expansion):
        super().__init__()
        self.embed_size = embed_size
        self.norm1 = nn.LayerNorm(embed_size)
        self.attention = nn.MultiheadAttention(embed_size, num_heads, add_bias_kv=True, batch_first=True)
        self.norm2 = nn.LayerNorm(embed_size)
        self.fc1 = nn.Linear(embed_size, forward_expansion * embed_size)
        self.fc2 = nn.Linear(forward_expansion * embed_size, embed_size)

    def forward(self, x):
        nx = self.norm1(x)
        a, _ = self.attention(nx, nx, nx, need_weights=False)
        x = x + a
        x = x + self.fc2(F.relu(self.fc1(self.norm2(x))))
        return x


class VisionTransformer(nn.Module):
    def __init__(self, image_shape, num_patches_along_dim, embed_size, num_classes, num_blocks=2, num_heads=2,
                 device=None, use_premade_positioning=False):
        super().__init__()
        # this is (channels, height, width)
        self.image_shape = image_shape
        self.num_patches_along_dim = num_patches_along_dim
        self.embed_size = embed_size
        self.use_premade_positioning = use_premade_positioning
        self.device = device if device is not None else torch.device(
            'cuda') if torch.cuda.is_available() else torch.device('cpu')

        assert image_shape[1] % num_patches_along_dim == 0, "need the patches to be divisible"

        # each patch is (channels, width/n, height/n)
        self.patch_shape = (
            self.image_shape[0], self.image_shape[1] // num_patches_along_dim,
            self.image_shape[2] // num_patches_along_dim)

        in_size = self.patch_shape[0] * self.patch_shape[1] * self.patch_shape[2]

        self.token_mapper = nn.Linear(in_size, self.embed_size)

        self.classification_token = nn.Parameter(torch.rand(1, self.embed_size))

        if not use_premade_positioning:
            self.raw_positions = torch.linspace(0, self.num_patches_along_dim**2, steps=self.num_patches_along_dim**2+1, dtype=torch.long).to(self.device)
            self.positional_embedding = nn.Embedding(self.num_patches_along_dim ** 2+1, self.embed_size)
        else:
            self.register_buffer("positional_embedding", self.get_positional_embeddings(), persistent=False)

        self.blocks = nn.Sequential(*[
            TransformerBlock(self.embed_size, num_heads, 2)
            for _ in range(num_blocks)
        ])

        self.fc1 = nn.Linear(self.embed_size, num_classes)
        self.softmax = nn.Softmax(1)

    def get_positional_embeddings(self):
        result = torch.ones(self.num_patches_along_dim ** 2 + 1, self.embed_size)
        for i in range(self.num_patches_along_dim ** 2 + 1):
            for j in range(self.embed_size):
                if j % 2 == 0:
                    result[i][j] = np.sin(i / (10000 ** (j / self.embed_size)))
                else:
                    result[i][j] = np.cos(i / (10000 ** ((j - 1) / self.embed_size)))
        return result

    def to_patches(self, images):
        batch_size, channels, height, width = images.shape
        assert height == width, "square image only"

        total_amount_of_patches = self.num_patches_along_dim ** 2

        patches = torch.zeros(batch_size, total_amount_of_patches, (height // self.num_patches_along_dim) ** 2)

        # # height and width is same
        patch_length = height // self.num_patches_along_dim

        for i, image in enumerate(images):
            for x in range(self.num_patches_along_dim):
                for y in range(self.num_patches_along_dim):
                    patch = image[:, x * patch_length:(x + 1) * patch_length,
                            y * patch_length:(y + 1) * patch_length]
                    patches[i, x * self.num_patches_along_dim + y] = torch.flatten(patch)
        return patches.to(self.device)

    def forward(self, image_batch):
        patches = self.to_patches(image_batch)
        tokens = self.token_mapper(patches)
        tokens = torch.cat((self.classification_token.expand(len(image_batch), 1, -1), tokens), dim=1)

        if self.use_premade_positioning:
            positions = self.positional_embedding.repeat(len(image_batch), 1, 1)
        else:
            positions = self.positional_embedding(self.raw_positions).unsqueeze(0).repeat(len(image_batch), 1, 1)

        combined = tokens + positions

        encoded = self.blocks(combined)

        classification = encoded[:, 0]

        out = self.softmax(self.fc1(classification))
        return out
